# [Flask Monitoring Dashboard Boilerplate]

> This project is inspired from Template [boilerplate code](https://appseed.us/boilerplate-code) used by [AppSeed](https://appseed.us). I have filtered out the boilerplate as per my needs and have added some API features - Features:

<br />

- `Up-to-date dependencies`
- `DBMS`: SQLite, PostgreSQL (production) 
- `DB Tools`: SQLAlchemy ORM, Flask-Migrate (schema migrations)
- `Session-Based` authentication, **Blueprints**
- `Deployment`: **Docker, docker-compose**, Gunicorn / Nginx, `HEROKU`

<br />

> Links

- 👉 [Flask Monitor Dashboard](https://sushanku-monitor-app.herokuapp.com//) - LIVE Demo

<br />

## ✨ Quick Start in `Docker`

> Get the code

```bash
$ git clone https://gitlab.com/sushanku/sushanku-monitor-app.git
$ cd sushanku-monitor-app
```

> Start the app in Docker

```bash
$ docker-compose up --build 
```

Visit `http://localhost:85` in your browser. The app should be up & running.

<br />

## ✨ **[Product Roadmap]

- [x] **Updated dependencies**
- [x] **Pythonic Footprint**
- [x] **Improved Authentication**: 
  - [x] `Password reset`
  - [x] `Email confirmation on register`
  - [x] `Extended user model`
    - [x] First, Last Name
    - [x] Birthday, Gender, Email, Phone
    - [x] User Photo 
- [x] `API` via Flask-RestX
- [x] `Sample Charts`
  - Main Dashbord updated to show case real data

<br />

## ✨ How to use it

> 👉 **Clone Sources** (this repo)

```bash
$ git clone https://gitlab.com/sushanku/sushanku-monitor-app.git
$ cd sushanku-monitor-app
```

<br />

> 👉 **Install Modules** using a Virtual Environment

```bash
$ virtualenv env
$ source env/bin/activate
$ pip3 install -r requirements.txt
```

Or for **Windows-based Systems**

```bash
$ virtualenv env
$ .\env\Scripts\activate
$
$ # Install modules - SQLite Database
$ pip3 install -r requirements.txt
```

<br />

> 👉 **Set up the environment**

```bash
$ export FLASK_APP=run.py
$ export FLASK_ENV=development
```

Or for **Windows-based Systems**

```bash
$ # CMD terminal
$ set FLASK_APP=run.py
$ set FLASK_ENV=development
$
$ # Powershell
$ $env:FLASK_APP = ".\run.py"
$ $env:FLASK_ENV = "development"
```

<br />

> 👉 **Start the APP**

```bash
$ flask run 
```

At this point we should be able to register new users, authenticate and access the API: 

- Register: `http://localhost:5000/register`
- Login: `http://localhost:5000/login`
- Access the public API
  - Get all items order by day: `http://localhost:5000/api/fileinfos/day`
  - Get all items order by week: `http://localhost:5000/api/fileinfos/week`
  - Get all items order by month: `http://localhost:5000/api/fileinfos/month`
  - Get all items: `http://localhost:5000/api/fileinfos/`
  - Upload files(text,pdf,images,etc): `http://localhost:5000/api/file_upload`

<br />


## ✨ Code-base structure

The project is coded using blueprints, app factory pattern, dual configuration profile (development and production) and an intuitive structure presented bellow:

- `run.py` loads the `.env` file
- Initialize the app using the specified profile: *Debug* or *Production*
  - If env.DEBUG is set to *True* the SQLite storage is used
  - If env.DEBUG is set to *False* the specified DB driver is used (MySql, PostgreSQL)
- Call the app factory method `create_app` defined in app/__init__.py
- Redirect the guest users to Login page
- Unlock the pages served by *home* blueprint for authenticated users

<br />
